package at.hakwt.swp4.example.violation;

public class LiskovViolationMain {

    public static void main(String[] args) {
        Rectangle r = new Rectangle();
        r.setHeight(10);
        r.setWidth(20);
        System.out.println(r.getClass().getName() + ": " + (r.getHeight() == 10));

        Rectangle s = new Square();
        s.setHeight(10);
        s.setWidth(20);

        // false !!
        System.out.println(s.getClass().getName() + ": " + (s.getHeight() == 10));

    }
}
