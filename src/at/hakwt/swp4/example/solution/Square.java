package at.hakwt.swp4.example.solution;

public class Square extends FourSidedShape {

    private final int a;

    public Square(int a) {
        this.a = a;
    }

    @Override
    public int getSideA() {
        return a;
    }

    @Override
    public int getSideB() {
        return a;
    }

    @Override
    public int getSize() {
        return a * a;
    }

}
