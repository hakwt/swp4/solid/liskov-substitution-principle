package at.hakwt.swp4.example.solution;

public class LiskovSolutionMain {

    public static void main(String[] args) {
        FourSidedShape r = new Rectangle(10, 20);
        System.out.println(r.getClass().getName() + ":  " + (r.getSideA() == 10));
        System.out.println(r.getClass().getName() + " size =  " + (r.getSize()));

        FourSidedShape s = new Square(10);
        System.out.println(s.getClass().getName() + ": " + (s.getSideA() == 10));
        System.out.println(s.getClass().getName() + " size = " + (s.getSize()));
    }

}
