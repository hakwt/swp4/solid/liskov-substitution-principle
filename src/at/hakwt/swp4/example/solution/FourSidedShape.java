package at.hakwt.swp4.example.solution;

/**
 *
 */
public abstract class FourSidedShape {

    public abstract int getSideA();

    public abstract int getSideB();

    public abstract int getSize();

}
