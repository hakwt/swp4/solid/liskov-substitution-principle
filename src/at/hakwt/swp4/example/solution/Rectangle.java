package at.hakwt.swp4.example.solution;

public class Rectangle extends FourSidedShape  {

    private final int a;

    private final int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int getSideA() {
        return a;
    }

    @Override
    public int getSideB() {
        return b;
    }

    @Override
    public int getSize() {
        return a * b;
    }

}
