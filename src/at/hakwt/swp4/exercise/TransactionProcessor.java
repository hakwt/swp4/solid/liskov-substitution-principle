package at.hakwt.swp4.exercise;

import java.math.BigDecimal;

public class TransactionProcessor {

    public void processWithdrawals(Account account, BigDecimal withdrawal) {
        account.withdraw(withdrawal);
    }

    public void processDeposit(Account account, BigDecimal deposit) {
        account.deposit(deposit);
    }

}
