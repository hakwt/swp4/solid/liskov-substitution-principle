package at.hakwt.swp4.exercise;

import java.math.BigDecimal;

/**
 * Repräsentiert ein Ansparkonto, bei dem nur angespart und nach einer
 * bestimmten Laufzeit aufgelöst wird. Abhebungen sind nicht möglich.
 */
public class FixedTermDepositAccount extends Account {

    private BigDecimal balance;

    public FixedTermDepositAccount() {
        this.balance = new BigDecimal(0);
    }

    @Override
    public void deposit(BigDecimal amount) {
        this.balance = this.balance.add(amount);
    }

    @Override
    public void withdraw(BigDecimal amount) {
        throw new UnsupportedOperationException("Withdrawals not allowed for FixedTermDepositAccounts");
    }

    @Override
    public BigDecimal getBalance() {
        return balance;
    }
}
