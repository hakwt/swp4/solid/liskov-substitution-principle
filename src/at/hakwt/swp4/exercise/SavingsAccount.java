package at.hakwt.swp4.exercise;

import java.math.BigDecimal;

/**
 * Represents an Online-Sparbuch
 *
 */
public class SavingsAccount extends Account {

    private BigDecimal currentBalance;

    public SavingsAccount() {
        this.currentBalance = new BigDecimal(0);
    }

    @Override
    public void deposit(BigDecimal amount) {
        currentBalance = currentBalance.add(amount);
    }

    @Override
    public void withdraw(BigDecimal amount) {
        if (currentBalance.subtract(amount).doubleValue() < 0.0d ) {
            throw new RuntimeException("Cannot go below 0!");
        }
        currentBalance = currentBalance.subtract(amount);

    }

    @Override
    public BigDecimal getBalance() {
        return currentBalance;
    }
}
