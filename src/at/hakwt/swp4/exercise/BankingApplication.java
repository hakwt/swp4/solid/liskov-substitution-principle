package at.hakwt.swp4.exercise;

import java.math.BigDecimal;

public class BankingApplication {

    public static void main(String[] args) {
        TransactionProcessor transactionProcessor = new TransactionProcessor();

        Account s1 = new SavingsAccount();
        transactionProcessor.processDeposit(s1, new BigDecimal(1000));
        transactionProcessor.processWithdrawals(s1, new BigDecimal(10));
        System.out.println("s1: " + s1.getBalance());

        Account c1 = new CurrentAccount(new BigDecimal(1000));
        transactionProcessor.processDeposit(c1, new BigDecimal(500));
        transactionProcessor.processWithdrawals(c1, new BigDecimal(1000));
        System.out.println("c1: " + c1.getBalance());

        // LSP violation
        Account f1 = new FixedTermDepositAccount();
        transactionProcessor.processDeposit(f1, new BigDecimal(1000));
        transactionProcessor.processWithdrawals(f1, new BigDecimal(100));
        System.out.println("f1: " + f1.getBalance());

    }

}
