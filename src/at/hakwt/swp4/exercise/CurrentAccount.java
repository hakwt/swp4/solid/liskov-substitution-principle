package at.hakwt.swp4.exercise;

import java.math.BigDecimal;

/**
 * Represents a Girokonto
 */
public class CurrentAccount extends Account {

    private BigDecimal balance;

    private final BigDecimal overdraft;

    public CurrentAccount(BigDecimal overdraft) {
        this.balance = new BigDecimal(0);
        this.overdraft = overdraft;
    }

    @Override
    public void deposit(BigDecimal amount) {
        this.balance = balance.add(amount);
    }

    @Override
    public void withdraw(BigDecimal amount) {
        if (balance.subtract(amount).doubleValue() < overdraft.negate().doubleValue() ) {
            throw new RuntimeException("Cannot go below overdraft!");
        }
        balance = balance.subtract(amount);
    }

    @Override
    public BigDecimal getBalance() {
        return balance;
    }
}
