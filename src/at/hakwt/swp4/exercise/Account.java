package at.hakwt.swp4.exercise;

import java.math.BigDecimal;

public abstract class Account {

    /**
     * Put the given amount on the account
     *
     * @param amount
     */
    public abstract void deposit(BigDecimal amount);

    /**
     * Reduces the balance of the account by the specified amount
     * provided given amount > 0 and account meets minimum available
     * balance criteria.
     *
     * @param amount
     */
    public abstract void withdraw(BigDecimal amount);

    public abstract BigDecimal getBalance();

}
