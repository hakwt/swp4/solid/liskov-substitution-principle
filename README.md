# Liskov substitution principle

## Übung

Im Package `at.hakwt.swp4.exercise` findet man eine kleine Banken-Anwendung. Es gibt drei verschiedene Typen von
Konten und eine abstrakte Klasse `Account` die definiert, was man mit einem Konto alles machen kann. Die Klasse 
`TransactionsProcessor` simuliert eine asynchrone Transaktionsverarbeitung wie sie bei Banken teilweise noch auf einem
Großrechner im Einsatz ist. 

Die main()-Methode der Klasse `BankingApplication` lässt sich ausführen, dabei kommt es allerdings zu einer
`UnsupportedOperationException`, weil der Kontotyp `FixedTermDepositAccount` das Liskovsche Substitutionsprinzip
verletzt.

Ändere die Struktur der Account-Klassen so, dass das LSP nicht verletzt wird. Kleine Änderungen an der Klasse
`TransactionProcessor` sind erlaubt.